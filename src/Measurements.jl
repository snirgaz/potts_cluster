module Measurements

export Obs,ObsMagnetization,dump_measurements,clear_measurements!,MeasurementArray,measure_all!,done_measurements,update_num_measurements!

using ..LatticeData
using ..Defs
using JLD
# Obsevable

abstract Obs

type ObsData{T}
  name::String
  mean::T
  num_of_measure::Int64
  c_num_of_measure::Int64
  #ObsData(name::String,num_of_measure::Int64)=new(Array{T,1}[],name,num_of_measure)
end

#typealias ObsDataArray{D} ObsData{Array{Float64,D}}
#typealias ObsDataScalar ObsData{Float64}

function ObsDataArray{D}(name::String,num_of_measure::Int64,dims::NTuple{D,Int64})
  ObsData{Array{Float64,D}}(name,zeros(dims),num_of_measure,0)
end

function ObsDataScalar(name::String,num_of_measure::Int64)
  ObsData{Float64}(name,0.0,num_of_measure,0)
end


function take_measurement!(obs_data::ObsData{Float64},data::Float64)
  if obs_data.c_num_of_measure==obs_data.num_of_measure
    return nothing
  end
  obs_data.c_num_of_measure+=1
  obs_data.mean+=(data-obs_data.mean)/float(obs_data.c_num_of_measure)
  return nothing
end

function clear_measurement!(obs_data::ObsData{Float64})
  obs_data.c_num_of_measure=0
  obs_data.mean=0.0
end


function take_measurement!{D}(obs_data::ObsData{Array{Float64,D}},data::Array{Float64,D})
  if obs_data.c_num_of_measure==obs_data.num_of_measure
    return
  end
  obs_data.c_num_of_measure+=1
  for i in CartesianRange(size(obs_data.mean))
    obs_data.mean[i]=obs_data.mean[i]+(data[i]-obs_data.mean[i])/float(obs_data.c_num_of_measure)
  end
  #@show obs_data.mean
end

function clear_measurement!{D}(obs_data::ObsData{Array{Float64,D}})
  obs_data.c_num_of_measure=0
  fill!(obs_data.mean,0.0)
end


function done_measurement{T}(obs_data::ObsData{T})
  obs_data.c_num_of_measure==obs_data.num_of_measure
end

function done_measurement{T,N}(obs_data::NTuple{N,ObsData{T}})
  obs_data[1].c_num_of_measure==obs_data[1].num_of_measure
end

function get_measurement{T}(obs_data::ObsData{T},meas_data::Dict{String,Float64})
  meas_data[m.obs_data.name]=m.obs_data.mean
end

function get_measurement{T,N}(obs_data::NTuple{N,ObsData{T}},meas_data::Dict{String,Float64}  )
  for o in obs_data
    meas_data[o.name]=o.mean
  end
end

function write_measurement{T}(obs_data::ObsData{T},file)
  write(file,obs_data.name,obs_data.mean)
end

function write_measurement{T,N}(obs_data::NTuple{N,ObsData{T}},file)
  for o in obs_data
    write(file,o.name,o.mean)
  end
end

function min_precent_measurement{T}(obs_data::ObsData{T})
  float(o.c_num_of_measure/o.num_of_measure)
end

function min_precent_measurement{T,N}(obs_data::NTuple{N,ObsData{T}})
  p=1.0
  return p
  for o in obs_data
    c_p=float(o.c_num_of_measure/o.num_of_measure)
    if c_p<p
      p=c_p
    end
  end
  p
end

# Measurements

type MeasurementArray
  measurements::Array{Obs,1}
  MeasurementArray(measurements::Array{Obs,1})=new(measurements)
end

function measure_all!(measurements::MeasurementArray,sim)
  for m in measurements.measurements
    measure_obs!(m,sim)
  end
end

function get_measurements(m_array::MeasurementArray)
  meas_data=Dict{String,Float64}()
  for m in m_array.measurements
    get_measurement(m.obs_data,meas_data)
  end
  meas_data
end

function read_measurements!(m_array::MeasurementArray)
  measure_file=joinpath(sim_path,"Measure.jld")
  jldopen(measure_file, "r") do file
    data=read(measure_file)
    for (key, value) in data
         for m in m_array.measurements
           if key==m.obs_data.obs_name
             m.obs_data=value
           end
         end
    end
  end
end

function dump_measurements(m_array::MeasurementArray,sim_path::String,out_file::IOStream)
  measure_file=joinpath(sim_path,"Measure.jld")
  jldopen(measure_file, "w") do file
    for m in m_array.measurements
      write_measurement(m.obs_data, file)
    end
  end
  p_done=min_precent_measurements(m_array)
  write(out_file,"Done - "*string(p_done*100)*" % \n")
  flush(out_file)
end

function clear_measurements!(m_array::MeasurementArray)
  for m in m_array.measurements
     clear_measurement!(m.obs_data)
  end
end

function update_num_measurements!(m_array::MeasurementArray,num_of_measure::Int64)
  for m in m_array.measurements
     m.obs_data.num_of_measure=num_of_measure
  end
end

function min_precent_measurements(m_array::MeasurementArray)
  p=1.0
  for m in m_array.measurements
    c_p=min_precent_measurement(m.obs_data)
    if c_p<p
      p=c_p
    end
  end
  return p
end

function done_measurements(m_array::MeasurementArray)
  for m in m_array.measurements
    if !done_measurement(m.obs_data)
      return false
    end
  end
  return true
end


# Kinetic energy
type ObsMagnetization <:Obs
  obs_data::NTuple{4,ObsData{Float64}}
  cis_q::NTuple{QPOTTS,Complex{Float64}}
  mag_et::Array{Complex{Float64},1}
  function ObsMagnetization(sim_params)
    new(tuple(ObsDataScalar("M2",sim_params.num_of_measure)
          ,ObsDataScalar("M4",sim_params.num_of_measure)
          ,ObsDataScalar("M2ET",sim_params.num_of_measure)
          ,ObsDataScalar("M4ET",sim_params.num_of_measure)
          )
        ,tuple([cis(2*pi*q/QPOTTS) for q in range(0,QPOTTS)]...)
        ,zeros(sim_params.M)
        )
  end
end

function measure_obs!(magnetization::ObsMagnetization,sim)
  tot_m=0.0+0.0im
  fill!(magnetization.mag_et,0.0+0.0im)
  for i in CartesianRange(size(sim.lat.q_state))
    tot_m+=magnetization.cis_q[sim.lat.q_state[i]]
    magnetization.mag_et[i[NDIMS]]+=magnetization.cis_q[sim.lat.q_state[i]]
  end
  take_measurement!(magnetization.obs_data[1],abs(tot_m)^2)
  take_measurement!(magnetization.obs_data[2],abs(tot_m)^4)
  mean_sqr=0.0
  for m in magnetization.mag_et
        mean_sqr+=(1./length(magnetization.mag_et))*abs(m)^2
  end
  take_measurement!(magnetization.obs_data[3],mean_sqr)
  take_measurement!(magnetization.obs_data[4],mean_sqr^2)
  return nothing
end


end
