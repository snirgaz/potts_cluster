include("../src/Defs.jl")
include("../src/SimParameters.jl")
include("../src/LatticeData.jl")
include("../src/Interaction.jl")
using .Defs
if CANONICAL
  include("../src/WormMoveCanonical.jl")
else
  include("../src/WormMove.jl")
end

include("../src/Measurements.jl")
include("../src/Progress.jl")
include("../src/Simulation.jl")

##
using .SimParameters
using .WormMoveCanonical
using .Simulation
using .Measurements
using .LatticeData
using .Interaction
using JSON
using JLD

function tune_chemical_potential(sim,sim_dict)
    mu_low=-5.0
    mu_high=5.0
    done=false
    num_of_iter=0
    max_num_iter=10
    sim.measurements=MeasurementArray(Any[ObsParticleNumber(sim.sim_params)])
    c_mu=0.5*(mu_high+mu_low)
    while !done
        c_mu=0.5*(mu_high+mu_low)
        WormMove.update_chemical_potential(sim.worm_data,-c_mu)
        write(sim.out_file,"Current mu -- "*string(c_mu)*"\n")
        thermalize!(sim)
        run_time=@timed Simulation.run!(sim,false)
        write(sim.out_file,"Time -- "*string(run_time[2])*"\n")
        write(sim.out_file,"Allocated -- "*string(run_time[3])*"\n")
        meas=Measurements.get_measurements(sim.measurements)
        c_density=meas["ParticleNumber"]-sim_dict["L"]/2
        write(sim.out_file,"Particle Number -- "*string(meas["ParticleNumber"])*"\n")
        write(sim.out_file,"Hole Density -- "*string(c_density)*"\n")
        flush(sim.out_file)
        if c_density>-1.0
            mu_high=c_mu
        else
            mu_low=c_mu
        end
        if abs(c_density+1.0)<1E-2
            done=true
            write(sim.out_file,"mu converged \n")
            flush(sim.out_file)
        end
        num_of_iter+=1
        clear_measurements!(sim.measurements)
        if num_of_iter>max_num_iter
          write(sim.out_file,"Increasing number of Thermalization steps")
          sim.sim_params.num_of_thermal*=10
          mu_low=-5
          mu_high=0.0
          num_of_iter=0
          max_num_iter+=1
        end
    end
    return c_mu
end

function scan_n_mu(sim,sim_dict)
    mu_low=sim_dict["mu_low"]
    mu_high=-sim_dict["mu_high"]
    num_of_points=sim_dict["num_of_mu"]
    sim.measurements=MeasurementArray(Any[ObsParticleNumber(sim.sim_params)])
    mu_range=linspace(mu_low,mu_high,num_of_points)
    n=Float64[]
    for c_mu in mu_range
        WormMove.update_chemical_potential(sim.worm_data,-c_mu)
        write(sim.out_file,"Current mu -- $c_mu \n")
        thermalize!(sim)
        run_time=@timed Simulation.run!(sim,false)
        write(sim.out_file,"Time -- $(run_time[2]) \n")
        write(sim.out_file,"Allocated -- $(run_time[3]) \n")
        meas=Measurements.get_measurements(sim.measurements)
        c_density=meas["ParticleNumber"]-sim_dict["L"]/2
        p_number=meas["ParticleNumber"]
        write(sim.out_file,"Particle Number -- $p_number \n")
        write(sim.out_file,"Hole Density -- $c_density \n")
        flush(sim.out_file)
        push!(n,c_density)
        clear_measurements!(sim.measurements)
    end
    return Dict("params"=>sim_dict,
                "mu_range"=>collect(mu_range),
                "den"=>n
                )
    #den_vs_mu_file=joinpath(sim_dict["sim_path"],"den_vs_mu.h5")
    #save(den_vs_mu_file, "den", n, "mu", mu_range)
end


function run_scan_mu(sim_dict)
    sim_params=SimParameters.SimParams(sim_dict)
    sim=Simulation.Sim(sim_params)
    update_num_measurements!(sim.measurements,sim_dict["num_of_measure_mu"])
    scan_n_mu(sim,sim_dict)
end

function run_sim(sim_dict)
    sim_params=SimParameters.SimParams(sim_dict)
    sim=Simulation.Sim(sim_params)
    update_num_measurements!(sim.measurements,sim_dict["num_of_measure_mu"])
    if sim_dict["tune"]
      mu_tune=tune_chemical_potential(sim,sim_dict)
      sim_dict["mu"]=-mu_tune
      mu_file=joinpath(sim_dict["sim_path"],"mu.json")
      stringdata = JSON.json(Dict("mu"=>-mu_tune))
      open(mu_file, "w") do f
              write(f, stringdata)
      end
    end
    sim.measurements=MeasurementArray(Any[#ObsGreen(sim_params.num_of_measure,sim_params.L) # Green
                          KineticEnergy(sim.sim_params)# Kinetic Energy
                          ,PotentialEnergy(sim.sim_params)
                          ,DensityProfile(sim.sim_params)
                          ,ObsParticleNumber(sim.sim_params)
                          #,CurrentObs(sim.sim_params)
                          ]
                          )
    update_num_measurements!(sim.measurements,sim_dict["num_of_measure_run"])
    thermalize!(sim)
    write(sim.out_file,"Done Thermalization")
    flush(sim.out_file)
    #println("Done Thermalization")
    Simulation.run!(sim,true)
    meas=Measurements.get_measurements(sim.measurements)
    return meas
end


function run_sim_wo_tune(sim_dict)
    sim_params=SimParameters.SimParams(sim_dict)
    sim=Simulation.Sim(sim_params)
    sim.measurements=MeasurementArray(Any[#ObsGreen(sim_params.num_of_measure,sim_params.L) # Green
                          KineticEnergy(sim.sim_params)# Kinetic Energy
                          ,PotentialEnergy(sim.sim_params)
                          #,DensityProfile(sim.sim_params)
                          ,ObsParticleNumber(sim.sim_params)
                          #,CurrentObs(sim.sim_params)
                          ]
                          )
    update_num_measurements!(sim.measurements,sim_dict["num_of_measure_run"])
    thermalize!(sim)
    println("Done Thermalization")
    Simulation.run!(sim,false)
    meas=Measurements.get_measurements(sim.measurements)
    return meas
end


function run_input(in_file)
   sim_params=JSON.parsefile(in_file)
   println("Hi")
   if sim_params["scan_mu"]
       run_scan_mu(sim_params)
   else
       run_sim(sim_params)
   end
end
