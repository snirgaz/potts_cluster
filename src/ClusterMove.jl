module ClusterMove

export ClusterMoveData,cluster_move!

import RandomNumbers
using ..SimParameters
using ..Defs
using ..LatticeData
using Base.Cartesian


type ClusterMoveData
  add_bond_space_bweight::Float64
  add_bond_tau_bweight::Float64
  cluster_sites::Array{NTuple{NDIMS,Int64},1}
  perculating_sites::Array{NTuple{NDIMS,Int64},1}
  old_state::Int64
  new_state::Int64
  rng::RandomNumbers.MersenneTwisters.MT19937
  function ClusterMoveData(sim_params::SimParams,rng::RandomNumbers.MersenneTwisters.MT19937)
    if sim_params.sim_type=="classical"
      add_bond_space_bweight=(1.0-exp(-sim_params.J))
      add_bond_tau_bweight=(1.0-exp(-sim_params.J))
    end
    if sim_params.sim_type=="quantum"
      eps=sim_params.beta/sim_params.M
      Jxy=eps*sim_params.J
      Jtau=-log((1.0/QPOTTS)*(exp(eps*sim_params.h)-1.0))
      add_bond_space_bweight=(1.0-exp(-Jxy))
      add_bond_tau_bweight=(1.0-exp(-Jtau))
    end
    new(add_bond_space_bweight # space add
      ,add_bond_tau_bweight # tau add
      ,NTuple{NDIMS,Int64}[] # cluste sites
      ,NTuple{NDIMS,Int64}[] # preculating sites
      ,1
      ,1
      ,rng
       )
  end
end


@generated function perculate_nn_sites!(cluster_data::ClusterMoveData,c_site::NTuple{NDIMS,Int64},lat::Lattice)
  quote
  # iterate lr
  @nexprs 2 lr->
  # iterate dims
  @nexprs $NDIMS dir->
  begin
    # nn index
    nn=hop(c_site,dir,lr,size(lat.q_state))
    # check if old_state and not already in cluster
    if lat.q_state[nn...]==cluster_data.old_state && !lat.c_state[nn...]
      # draw add probability
      p=rand(cluster_data.rng)
      # tau or spatial
      add_prob= (dir==$NDIMS) ? cluster_data.add_bond_tau_bweight : cluster_data.add_bond_space_bweight
      if (p<add_prob)
        # add site to cluster
        lat.c_state[nn...]=true
        push!(cluster_data.cluster_sites,nn)
        push!(cluster_data.perculating_sites,nn)
        # flip
        lat.q_state[nn...]=cluster_data.new_state
      end
    end
  end
  end
end

"""
    cluster_move!(ClusterData,Lattice)

single shift move of worm's head
"""
function cluster_move!(cluster_data::ClusterMoveData,lat::Lattice)
  # clear cluster sites
  empty!(cluster_data.cluster_sites)
  # draw site
  site=ind2sub(size(lat.q_state),rand(cluster_data.rng,1:length(lat.q_state)))
  # add first site
  push!(cluster_data.cluster_sites,site)
  push!(cluster_data.perculating_sites,site)
  # old state
  cluster_data.old_state=lat.q_state[site...]
  # new state
  cluster_data.new_state=mod1(cluster_data.old_state+rand(cluster_data.rng,1:(QPOTTS-1)),QPOTTS)
  # flip new state
  lat.q_state[site...]=cluster_data.new_state
  # mark site
  lat.c_state[site...]=true
  # continue while perculating sites is not empty
  while !isempty(cluster_data.perculating_sites)
    # pop c_site
    c_site=pop!(cluster_data.perculating_sites)
    perculate_nn_sites!(cluster_data,c_site,lat)
  end
  # unmark sites
  for ind in cluster_data.cluster_sites
    lat.c_state[ind...]=false
  end
end


end # module
