module Progress

export ProgressData,update_rate
using ..SimParameters

type ProgressData
  initial_time::DateTime
  last_dump_time::DateTime
  delta_dump::Float64
  moves_between_dump::Int64
  function ProgressData(sim_params::SimParams)
    new(now()
        ,now()
        ,sim_params.delta_dump
        ,10)
  end
end

function update_rate(progres_data::ProgressData,out_file::IOStream)
  current_time=now()
  time_diff=convert(Float64,Dates.value(current_time-progres_data.last_dump_time))/(1000.0*3600.0)
  ratio=progres_data.delta_dump/(time_diff+1E-12)
  ratio=ratio>10.0 ? 10.0 : ratio
  progres_data.moves_between_dump=convert(Int64,floor(progres_data.moves_between_dump*ratio))
  progres_data.last_dump_time=current_time
  write(out_file,"Elapsed Time -- $time_diff Hours \n")
  moves_between_dump=progres_data.moves_between_dump
  write(out_file,"Number of Move until next dump $moves_between_dump \n")
  flush(out_file)
end

end
