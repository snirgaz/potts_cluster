module SimParameters

export SimParams

# simulation params
type SimParams
  # sim_path
  sim_path::String
  input_file::String
  delta_dump::Float64
  # Model parameters for the action
  sim_type::String
  beta::Float64 # inverse temprature
  J::Float64 # Potts coupling
  h::Float64 # transverse field
  # L
  L::Int64
  # M
  M::Int64
  # num of thermalization steps
  num_of_thermal::Int64
  # num of sweeps berween measurements
  num_of_sweeps::Int64
  # num of measurements
  num_of_measure::Int64
  # rng seed
  seed::Int64
  # unintizlied constructor
  SimParams()=new()
end

# Constructor from dictionary

function SimParams(params::Dict{String,Any})
  newSimParams=SimParams()
  for f in keys(params)
    if haskey(params,f)
      setfield!(newSimParams, Symbol(f), params[f])
    else
      println("Missing key $f")
    end
  end
  return newSimParams
end




end  # module
