include("../src/Defs.jl")
include("../src/SimParameters.jl")
include("../src/LatticeData.jl")
include("../src/ClusterMove.jl")
include("../src/Measurements.jl")
include("../src/Progress.jl")
include("../src/Simulation.jl")

##
using .SimParameters
using .ClusterMove
using .Simulation
using .Measurements
using .LatticeData
using JSON


function run_sim(sim_dict)
    sim_params=SimParameters.SimParams(sim_dict)
    sim=Simulation.Sim(sim_params)
    thermalize!(sim)
    println("Done Thermalization")
    Simulation.run!(sim,false)
    meas=Measurements.get_measurements(sim.measurements)
    return meas
end

function run_input(in_file)
   sim_params=JSON.parsefile(in_file)
   run_sim(sim_params)
end
