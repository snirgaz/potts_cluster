module Simulation

export Sim,thermalize!

using ..Defs
using ..SimParameters
using ..LatticeData
using ..Measurements
using ..Progress
using ..ClusterMove
import RandomNumbers
import RandomNumbers.MersenneTwisters

type Sim
  lat::Lattice
  cluster_data::ClusterMoveData
  sim_params::SimParams
  measurements::MeasurementArray
  progres_data::ProgressData
  out_file::IOStream
  rng::RandomNumbers.MersenneTwisters.MT19937
  function Sim(sim_params::SimParams)
    rng=RandomNumbers.MersenneTwisters.MT19937(sim_params.seed)
    lat=Lattice(sim_params)
    new(
      lat # Lattice
      ,ClusterMoveData(sim_params,rng) # Worm Move
      ,sim_params # simulation parameters
      ,MeasurementArray(Obs[
                            #ObsGreen(sim_params.num_of_measure,sim_params.L) # Green
                            ObsMagnetization(sim_params)# Kinetic Energy
                            ]
                            )
      ,ProgressData(sim_params)
      ,open(joinpath(sim_params.sim_path,"out.txt"),"w")
      ,rng
    )
  end
end


function next_move(sim::Sim)
  cluster_move!(sim.cluster_data,sim.lat)
end

function thermalize!(sim::Sim)
  for i in 1:sim.sim_params.num_of_thermal
     next_move(sim)
  end
end


function run!(sim::Sim,dump::Bool)
  # check if done flag
  not_done=true
  # move counter between measurement dump
  #c_move=0
  while not_done
    # number of sweeps
    for c_sweep in 1:sim.sim_params.num_of_sweeps
      next_move(sim)
    end
    measure_all!(sim.measurements,sim)
    not_done=!done_measurements(sim.measurements)
    # c_move+=1
    # # dump
    # if c_move>sim.progres_data.moves_between_dump
    #   update_rate(sim.progres_data,sim.out_file)
    #   if dump
    #     dump_measurements(sim.measurements,sim.sim_params.sim_path,sim.out_file)
    #     #dump_lattice(sim.lat,sim.sim_params.sim_path,sim.out_file)
    #   end
    #   c_move=0
    # end
  end
  if dump
    dump_measurements(sim.measurements,sim.sim_params.sim_path,sim.out_file)
    #dump_lattice(sim.lat,sim.sim_params.sim_path,sim.out_file)
    write(sim.out_file,"Done Measurements \n")
    flush(sim.out_file)
  end
  return nothing
end

end
