using ArgParse

include("./RunSim.jl")

s = ArgParseSettings()
@add_arg_table s begin
    "--input_file"
        help = "input_file_name"
end

parsed_args = parse_args(ARGS, s)

run_input(parsed_args["input_file"])
