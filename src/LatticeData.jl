module LatticeData

export Lattice,hop,dump_lattice
using ..Defs
using ..SimParameters
using Base.Cartesian
using JLD

type Lattice
  # Potts state
  q_state::Array{Int64,NDIMS}
  # cluster flag sites
  c_state::Array{Bool,NDIMS}
  # spatial size
  L::Int64
  # temporal Size
  M::Int64
  function Lattice(sim_params::SimParams)
    new(
     ones(Int64,(sim_params.L*ones(Int64,NDIMS-1)...,sim_params.M))
    ,falses(sim_params.L*ones(Int64,NDIMS-1)...,sim_params.M) # plaquette state
    ,sim_params.L # system size
    ,sim_params.M # temporal size
  )
  end
end


@generated function hop(index::NTuple{NDIMS,Int64},dir::Int64,lr::Int64,dims::NTuple{NDIMS,Int64})
    quote
        @nif 2 lr_d->(lr==lr_d) lr_d->
        begin
            @nif $NDIMS d->(d==dir) d->
            begin
                @ntuple $NDIMS d_t->
                begin
                    if d_t==d
                      if (lr_d==1)
                        (index[d_t]==dims[d_t] ? 1 : index[d_t]+1)
                      else
                        (index[d_t]==1 ? dims[d_t] : index[d_t]-1)
                      end
                    else
                      index[d_t]
                    end
                end
            end
        end
    end
end

function test_configuration(lat::Lattice)
  return true
end


end # Lattice module
